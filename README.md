# Python Backend project NaNLABS

_This project follows NaNLABS's challenge specifications and  allows you to create cards in Trello._

_Developed in Python with Django REST Framework._


In order to run/test the project follow the next steps:

| :warning: IMPORTANT :warning: |
| ---------- |
| Make sure you have an account in [Trello](https://trello.com/) and you are a member of this [dashboard](https://trello.com/b/2k1n4gDn/project-1). If you are not a member, you can become one by clicking [here](https://trello.com/invite/b/2k1n4gDn/43c0846e4897dcda2ffe0f1bb0d7dd6b/project-1). |

1) First, please follow the [installation guide](https://gitlab.com/paula.giammarini/nanlabs/-/wikis/Space-X's-installation-Guide) to get everything you need.

2) After the installation, you are able to run/test the project. Instructions [here](https://gitlab.com/paula.giammarini/nanlabs/-/wikis/Space-X's-Test-Guide)

To know about the Error Codes click [here](https://gitlab.com/paula.giammarini/nanlabs/-/wikis/Error-Codes)


All the information you need is available in our Wiki.
