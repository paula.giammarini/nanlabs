ACTUAL_PROJECT = 'Project 1'
API_SPACEX_KEY = 'e493613b2c5867ec53884057ebb204dd'
BUG = 'Bug'
MAINTENANCE = 'Maintenance'
OTHER = 'Other'
RESEARCH = 'Research'
TEST = 'Test'
TO_DO = 'To Do'

# Replace xx with the generated token before start testing the project.
# The token must be enclosed in single inverted commas ('')
API_SPACEX_TOKEN = 'xx'

# Ideally the token would be manage by API trello's authorization system.
# This token sould be secret and belongs to a specific user.

CATEGORIES = [
   MAINTENANCE,
   RESEARCH,
   TEST
]

PARAMS = {
   'key': API_SPACEX_KEY,
   'token': API_SPACEX_TOKEN,
}

WORDS = [
   'problem',
   'fix',
   'error',
   'disaster',
   'catastrophe'
]

FIELDS = {
   'fields': [
      'id',
      'name'
   ]
}

GET_PARAMS = dict(PARAMS, **FIELDS)

LISTS = [
   TO_DO,
   OTHER
]