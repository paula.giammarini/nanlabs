from drf_yasg.utils import swagger_auto_schema

from rest_framework import status, viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from spaceX.documentations import doc_card_create
from spaceX.serializers import CardSerializerIn
from spaceX.utils import Services


class CardViewSet(viewsets.GenericViewSet):
    permission_classes = [AllowAny]
    http_method_names = ['post']
        
    @swagger_auto_schema(**doc_card_create)
    def create(self, request):
        serializer = CardSerializerIn(data=request.data)
        serializer.is_valid(raise_exception=True)
        dashboard_id, members = Services.get_dashboard()
        labels = Services.get_labels(dashboard_id)
        list_to_do, list_other = Services.get_lists(dashboard_id)
        data = Services.prepare_data(
            serializer.data,
            labels,
            list_to_do,
            list_other,
            members
        )
        new_card = Services.create_new_card(data)
        return Response(new_card, status=status.HTTP_201_CREATED)
