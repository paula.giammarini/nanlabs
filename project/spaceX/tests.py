import json

from rest_framework.test import APITestCase

from spaceX.utils import Services


class ApiCardCreate(APITestCase):
    
    url = '/api/card/'
    
    def test_post_ok_bug(self):
        data = {
            'type': '    BuG   ',
            'description': 'Cockpit is not depressurizing correctly'
        }
        response = self.client.post(
            self.url,
            json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(len(response.data), 34)
        self.assertEqual(response.data['desc'], data['description'])
        self.assertEqual(response.data['name'][0:3], 'Bug')
        self.assertEqual(response.data['labels'][0]['name'], 'Bug')
        self.assertEqual(len(response.data['idMembers']), 1)

    def test_post_ok_issue(self):
        data = {
            'type': '   IssUE   ',
            'title': 'Send message',
            'description': 'Let pilots send messages to central'
        }
        response = self.client.post(
            self.url,
            json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(len(response.data), 34)
        self.assertEqual(response.data['desc'], data['description'])
        self.assertEqual(response.data['name'], 'Send message')
        self.assertEqual(len(response.data['idMembers']), 0)
        dashboard = response.data['idBoard']
        list_to_do, list_other = Services.get_lists(dashboard)
        self.assertEqual(response.data['idList'], list_to_do)

    def test_post_ok_task(self):
        data = {
            'type': ' taSk  ',
            'title': 'Clean the rocket',
            'category': 'Maintenance'
        }
        response = self.client.post(
            self.url,
            json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(len(response.data), 34)
        self.assertEqual(response.data['name'], 'Clean the rocket')
        self.assertEqual(response.data['labels'][0]['name'], 'Maintenance')
        self.assertEqual(len(response.data['idMembers']), 0)

    def test_post_ok_fields(self):
        data = {
            'type': 'Bug',
            'description': 'Cockpit is not depressurizing correctly'
        }
        response = self.client.post(
            self.url,
            json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 201)
        self.assertContains(response, 'id', status_code=201)
        self.assertContains(response, 'desc', status_code=201)
        self.assertContains(response, 'name', status_code=201)
        self.assertContains(response, 'url', status_code=201)
        self.assertContains(response, 'labels', status_code=201)
        self.assertContains(response, 'idLabels', status_code=201)
        self.assertContains(response, 'idMembers', status_code=201)
        self.assertContains(response, 'idList', status_code=201)
        self.assertContains(response, 'idBoard', status_code=201)

    def test_post_fail_type(self):
        data = {
            'description': 'Description',
        }
        response = self.client.post(
            self.url,
            json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 400)
        self.assertContains(response, 'type', status_code=400)
        self.assertContains(response, 'This field is required', status_code=400)
        data['type'] = 'Other type'
        response = self.client.post(
            self.url,
            json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 400)
        self.assertContains(response, 'E405', status_code=400)

    def test_post_fail_bug(self):
        data = {
            'type': 'Bug',
        }
        response = self.client.post(
            self.url,
            json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 400)
        self.assertContains(response, 'E401', status_code=400)

    def test_post_fail_task(self):
        data = {
            'type': 'task',
        }
        response = self.client.post(
            self.url,
            json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 400)
        self.assertContains(response, 'E402', status_code=400)
        data['title'] = 'Title'
        response = self.client.post(
            self.url,
            json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 400)
        self.assertContains(response, 'E402', status_code=400)
        data['category'] = 'Other'
        response = self.client.post(
            self.url,
            json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 400)
        self.assertContains(response, 'E403', status_code=400)

    def test_post_fail_issue(self):
        data = {
            'type': 'issue',
        }
        response = self.client.post(
            self.url,
            json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 400)
        self.assertContains(response, 'E404', status_code=400)
        data_1 = {
            'type': 'issue',
            'title': "Send information"
        }
        response = self.client.post(
            self.url,
            json.dumps(data_1),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 400)
        self.assertContains(response, 'E404', status_code=400)
        data_2 = {
            'type': 'issue',
            'description': "Send information to the pilots"
        }
        response = self.client.post(
            self.url,
            json.dumps(data_2),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 400)
        self.assertContains(response, 'E404', status_code=400)