from django.apps import AppConfig


class spaceXConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'spaceX'
