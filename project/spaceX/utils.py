import json
import requests
from random import randint, choice

from spaceX.constants import (
    ACTUAL_PROJECT,
    BUG,
    GET_PARAMS,
    LISTS,
    PARAMS,
    TO_DO,
    WORDS,
)


class Services():
    
    @staticmethod
    def get_dashboard():
        url = 'https://api.trello.com/1/members/me/boards'
        fields = {
            'fields': GET_PARAMS['fields'] + ['memberships']
        }
        response = requests.request(
            "GET",
            url,
            params=dict(PARAMS, **fields),
        )
        response.raise_for_status()
        dashboards_list = json.loads((response.content))
        is_equal = lambda x: x['name']==ACTUAL_PROJECT
        work_dashboard = list(filter(is_equal, dashboards_list))[0]
        dashboard_id = work_dashboard['id']
        members = work_dashboard['memberships']
        return dashboard_id, members

    @staticmethod
    def get_lists(dashboard):
        url = 'https://api.trello.com/1/boards/'
        lists = requests.request(
            "GET",
            url + dashboard + "/lists/",
            params=GET_PARAMS,
        )
        lists.raise_for_status()
        lists = json.loads((lists.content))
        lists = list(
            filter(lambda x: (x['name'] in LISTS), lists)
        )
        if lists[0]['name'] == TO_DO:
            list_to_do = lists[0]['id']
            list_other = lists[1]['id']
        else:
            list_other = lists[0]['id']
            list_to_do = lists[1]['id']
        return list_to_do, list_other

    @staticmethod
    def get_labels(dashboard):
        url = 'https://api.trello.com/1/boards/'
        labels_list = requests.request(
            "GET",
            url + dashboard + "/labels/",
            params=GET_PARAMS
        )
        labels_list.raise_for_status()
        labels_list = json.loads((labels_list.content))
        return labels_list

    @staticmethod
    def create_new_card(data):
        card = requests.request(
            "POST",
            "https://api.trello.com/1/cards",
            params=dict(PARAMS, **data)
        )
        card.raise_for_status()
        card = json.loads((card.content))
        return card

    @staticmethod
    def prepare_data(data, labels, to_do, other, members):
        card_type = data['type'].strip().lower()
        card_data = {}
        if card_type == 'bug':
            card_data['name'] = (
                BUG
                + ' - '
                + choice(WORDS)
                + ' - '
                + str(randint(0, 50))
            )
            card_data['desc'] = data['description'].capitalize()
            card_data['idLabels'] = list(
                filter(lambda x: x['name']==BUG, labels)
            )[0]['id']
            card_data['idMembers'] = choice(members)['idMember']
            card_data['idList'] = other
        elif card_type == 'issue':
            card_data['idList'] = to_do
            card_data['name'] = data['title']
            card_data['desc'] = data['description'].capitalize()
        elif card_type == 'task':
            category  = data['category'].strip().title()
            card_data['name'] = data['title']
            labels = list(
                filter(lambda x: x['name']==category, labels)
            )
            card_labels = []
            for label in labels:
                card_labels.append(label['id'])
            card_data['idLabels'] = card_labels
            card_data['idList'] = other
        return card_data
