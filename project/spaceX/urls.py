from drf_yasg import openapi
from drf_yasg.views import get_schema_view

from django.urls import include, path
from django.conf.urls import url

from rest_framework import permissions, routers

from spaceX.api import CardViewSet


schema_view = get_schema_view(
    openapi.Info(
        title='Space X API',
        default_version='v1',
        description='Documentation Space X API',
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)


app_name = 'api'

router = routers.SimpleRouter()

router.register(r'card', CardViewSet, basename='card')


urlpatterns = [
    url(r'api/', include(router.urls)),
    path(
        'swagger/',
        schema_view.with_ui('swagger', cache_timeout=0),
    ),
]