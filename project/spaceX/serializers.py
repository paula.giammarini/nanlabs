from rest_framework import serializers

from spaceX.validations import Validator


class CardSerializerIn(serializers.Serializer):
    validator = Validator()
    type = serializers.CharField()
    description = serializers.CharField(required=False)
    category = serializers.CharField(required=False)
    title = serializers.CharField(required=False)

    def validate(self, data):
        self.validator.validate(data)
        return data
