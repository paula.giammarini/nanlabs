from rest_framework import serializers

from spaceX.constants import CATEGORIES


class Validator():
    
    def validate_type(self, data):
        type = data['type'].strip().lower()
        types = [
            'bug',
            'issue',
            'task'
        ]
        if type not in types:
            raise serializers.ValidationError('E405')


    def validate_params_correct(self, data):
        type = data['type'].strip().lower()
        description = data.get('description')
        category = data.get('category')
        title = data.get('title')
        if type == 'bug' and not description:
            raise serializers.ValidationError('E401')
        elif type == 'task':
            if not category or not title:
                raise serializers.ValidationError('E402')
            elif category.strip().title() not in CATEGORIES:
                raise serializers.ValidationError('E403')
        elif type == 'issue' and (not title or not description):
            raise serializers.ValidationError('E404')
        return data

    def validate(self, data):
        self.validate_type(data)
        self.validate_params_correct(data)
