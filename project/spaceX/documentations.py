from drf_yasg import openapi
from spaceX.serializers import CardSerializerIn

doc_card_create = {
    'responses': {
        '201': openapi.Response('Success.',),
        '400': 'Invalid Request.\n'
               '- Codigo E401: For type "Bug", description is required.\n'
               '- Codigo E402: For type "Task", category and title are required.\n'
               '- Codigo E403: The category is not valid. Try with: Maintenance, Research or Test.\n'
               '- Codigo E404: For type "Issue", title and description are required.\n'
               '- Codigo E405: Invalid type. Try with: Bug, Task or Issue.\n'
    },
    'operation_id': 'Registration of a card',
    'operation_description': 'Register a new card in the system.',
    'request_body': 
        openapi.Schema(
            type=openapi.TYPE_OBJECT,
            required=['type'],
            properties={
                'category': openapi.Schema(type=openapi.TYPE_STRING),
                'description': openapi.Schema(type=openapi.TYPE_STRING),
                'title': openapi.Schema(type=openapi.TYPE_STRING),
                'type': openapi.Schema(type=openapi.TYPE_STRING)

            },
        )
}
